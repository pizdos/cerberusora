﻿using System;
using CerberusOra.Authentication;
using CerberusOra.Routes;
using CerberusOra.SomeUser;
using Microsoft.Extensions.DependencyInjection;

namespace CerberusOra.App
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var services = new ServiceCollection();
            ConfigureServices(services);

            var serviceProvider = services.BuildServiceProvider();

            var app = serviceProvider.GetService<App>() ?? throw new Exception("App not found");
            app.Run(args);
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<App>();
            services.AddSingleton<ServiceRouter>();
            services.AddSingleton<TicketGrantingServiceRouter>();
            services.AddSingleton<AuthenticationServer>();
            services.AddSingleton<UserAuthService>();
        }
    }
}