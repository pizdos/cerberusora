﻿using System;
using System.Collections.Generic;
using CerberusOra.Authentication;
using CerberusOra.Routes;
using CerberusOra.SomeUser;
using CerberusOra.SomeUser.Abstractions;

namespace CerberusOra.App
{
    public class App
    {
        private readonly AuthenticationServer _authenticationServer;
        private readonly TicketGrantingServiceRouter _ticketGrantingServiceRouter;
        private readonly UserAuthService _userAuthService;

        public App(AuthenticationServer authenticationServer, TicketGrantingServiceRouter ticketGrantingServiceRouter,
            UserAuthService userAuthService)
        {
            _authenticationServer = authenticationServer;
            _ticketGrantingServiceRouter = ticketGrantingServiceRouter;
            _userAuthService = userAuthService;
        }

        public void Run(string[] args)
        {
            Console.WriteLine("fsd");
            var users = new List<User>(2)
            {
                new(Guid.Parse("a57fcc83-b0dd-4ef0-be8b-0690e58d7d18"), "Yuri", "1234"),
                new(Guid.Parse("f0067834-9ff7-4b1e-a256-01f798ac5dd6"), "Pidor", "1234")
            };

            _authenticationServer.Register(users);

            var stupidUser = users[0];
            var tryAuthService = _userAuthService.TryAuthService(stupidUser, "Vk");


            Console.WriteLine("Hello World!");
        }
    }
}