﻿using System;
using CerberusOra.Abstractions;
using CerberusOra.Abstractions.Models;
using CerberusOra.Helpers;
using CerberusOra.Service.Communication;
using CerberusOra.Tickets;

namespace CerberusOra.Service
{
    public class SomeService
    {
        public readonly Guid Id;

        public readonly string Name;

        public readonly byte[] Secret;

        private readonly Cache<UserAuthenticator> _userAuthenticatorCache = new();

        public SomeService(Guid id, string name)
        {
            Id = id;
            Name = name;
            Secret = Sha256Helpers.ComputeHash($"{Name}{Guid.NewGuid()}");
        }

        public UserServiceResponse Auth(UserServiceRequest request)
        {
            var serviceTicket = ServiceTicket.Decrypt(request.EncryptedServiceTicket, Secret);
            var userAuthenticator =
                UserAuthenticator.Decrypt(request.EncryptedUserAuthenticator, serviceTicket.ServiceSessionKey);

            if (!IsVerify(serviceTicket, userAuthenticator))
            {
                throw new Exception();
            }

            _userAuthenticatorCache.GetOrAdd(serviceTicket.UserId, userAuthenticator);

            IEncryptable serviceAuthenticator = new ServiceAuthenticator(Id, serviceTicket.Timestamp);
            return new UserServiceResponse(serviceAuthenticator.Encrypt(serviceTicket.ServiceSessionKey));
        }

        private bool IsVerify(ServiceTicket serviceTicket, UserAuthenticator userAuthenticator) =>
            serviceTicket.UserId == userAuthenticator.UserId && serviceTicket.Timestamp == userAuthenticator.Timestamp;
    }
}