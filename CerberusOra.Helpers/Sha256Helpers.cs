﻿using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace CerberusOra.Helpers
{
    public class Sha256Helpers
    {
        public static byte[] ComputeHash(string text)
        {
            return System.Security.Cryptography.SHA256.Create()
                .ComputeHash(Encoding.UTF8.GetBytes(text));
        }
    }
}