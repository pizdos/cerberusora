﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;

namespace CerberusOra.Helpers
{
    public class Cache<TItem>
    {
        private readonly IMemoryCache _cache = new MemoryCache(new MemoryCacheOptions()
        {
            SizeLimit = 1024
        });

        private readonly ConcurrentDictionary<object, SemaphoreSlim> _locks =
            new ConcurrentDictionary<object, SemaphoreSlim>();

        private readonly MemoryCacheEntryOptions _memoryCacheEntryOptions;

        public Cache()
        {
            _memoryCacheEntryOptions = new MemoryCacheEntryOptions()
            {
                Size = 1,
                Priority = CacheItemPriority.High,
                SlidingExpiration = TimeSpan.FromMinutes(60)
            };
        }

        public Cache(MemoryCacheEntryOptions memoryCacheEntryOptions)
        {
            _memoryCacheEntryOptions = memoryCacheEntryOptions;
        }

        public async Task<TItem> GetOrAdd(object key, TItem item)
        {
            if (_cache.TryGetValue(key, out TItem cacheEntry))
                return cacheEntry;

            var lockForKey = 
                _locks.GetOrAdd(key, k => new SemaphoreSlim(1, 1));
            await lockForKey.WaitAsync();

            try
            {
                if (!_cache.TryGetValue(key, out cacheEntry))
                {
                    cacheEntry = item;
                    _cache.Set(key, cacheEntry, _memoryCacheEntryOptions);
                }
            }
            finally
            {
                lockForKey.Release();
            }

            return cacheEntry;
        }
    }
}