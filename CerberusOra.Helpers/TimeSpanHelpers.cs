﻿using System;

namespace CerberusOra.Helpers
{
    public static class TimeSpanHelpers
    {
        public static TimeSpan Now()
        {
            var beginningOfTime = new DateTime(1970, 1, 9, 0, 0, 00);
            return DateTime.Now - beginningOfTime;
        }
        
        public static int NowSeconds()
        {
            var beginningOfTime = new DateTime(1970, 1, 9, 0, 0, 00);
            return (DateTime.Now - beginningOfTime).Seconds;
        }
    }
}