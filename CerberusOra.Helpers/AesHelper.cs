﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace CerberusOra.Helpers
{
    public class AesHelper
    {
        public static string Encrypt(string text, byte[] key)
        {
            var iv = new byte[16];

            using var aes = Aes.Create();
            aes.Key = key;
            aes.IV = iv;

            var encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

            using var memoryStream = new MemoryStream();
            using var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            using (var streamWriter = new StreamWriter(cryptoStream))
            {
                streamWriter.Write(text);
            }

            var array = memoryStream.ToArray();

            return Convert.ToBase64String(array);
        }

        public static string Decrypt(string text, byte[] key)
        {
            var iv = new byte[16];
            var buffer = Convert.FromBase64String(text);

            using var aes = Aes.Create();
            aes.Key = key;
            aes.IV = iv;
            var decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

            using var memoryStream = new MemoryStream(buffer);
            using var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            using var streamReader = new StreamReader(cryptoStream);
            return streamReader.ReadToEnd();
        }
    }
}