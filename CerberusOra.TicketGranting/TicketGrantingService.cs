﻿using System;
using System.Collections.Generic;
using CerberusOra.Abstractions;
using CerberusOra.Abstractions.Models;
using CerberusOra.Helpers;
using CerberusOra.TicketGranting.Communication;
using CerberusOra.Tickets;

namespace CerberusOra.TicketGranting
{
    public class TicketGrantingService
    {
        public readonly Guid Id;

        private readonly Dictionary<Guid, byte[]> _serviceSecret = new(10);

        private readonly Cache<UserAuthenticator> _userAuthenticatorCache = new();

        public readonly byte[] Secret = Sha256Helpers.ComputeHash(Guid.NewGuid().ToString());

        public TicketGrantingService(Guid id)
        {
            Id = id;
        }

        private static bool IsVerify(TicketGrantingServiceTicket ticket, UserAuthenticator userAuthenticator) =>
            ticket.UserId != userAuthenticator.UserId || ticket.Timestamp != userAuthenticator.Timestamp;

        public string Auth(UserTicketGrantingServiceRequest request)
        {
            var ticket = TicketGrantingServiceTicket.Decrypt(request.EncryptedTicket, Secret);
            if (ticket == null)
                throw new NullReferenceException();

            var ticketTicketGrantingServiceSessionKey = ticket.TicketGrantingServiceSessionKey;
            var userAuthenticator =
                UserAuthenticator.Decrypt(request.UserAuthenticator, ticketTicketGrantingServiceSessionKey);
            if (userAuthenticator == null)
                throw new NullReferenceException();

            if (IsVerify(ticket, userAuthenticator))
                throw new ArgumentException(); // fixme: implement specific exception or behavior

            _userAuthenticatorCache.GetOrAdd(userAuthenticator.UserId, userAuthenticator);

            var serviceSessionKey = Sha256Helpers.ComputeHash(Guid.NewGuid().ToString());
            IEncryptable serviceTicket = new ServiceTicket(
                userAuthenticator.UserId,
                request.ServiceId,
                userAuthenticator.Timestamp,
                request.Lifetime, serviceSessionKey
            );

            IEncryptable response = new UserTicketGrangtingServiceResponse(
                request.ServiceId,
                userAuthenticator.Timestamp,
                request.Lifetime,
                serviceSessionKey,
                serviceTicket.Encrypt(_serviceSecret[request.ServiceId])
            );

            return response.Encrypt(ticketTicketGrantingServiceSessionKey);
        }

        public void Register(Service.SomeService someService)
        {
            _serviceSecret.Add(someService.Id, someService.Secret);
        }

        public void Register(IEnumerable<Service.SomeService> services)
        {
            foreach (var service in services) Register(service);
        }
    }
}