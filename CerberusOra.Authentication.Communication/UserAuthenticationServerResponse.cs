﻿using System;
using CerberusOra.Abstractions;
using CerberusOra.Helpers;
using Newtonsoft.Json;

namespace CerberusOra.Authentication.Communication
{
    public class UserAuthenticationServerResponse : IEncryptable
    {
        public readonly Guid TicketGrantingServiceId;

        public readonly int Timestamp;

        public readonly int Lifetime;

        public readonly byte[] TicketGrantingServiceSessionKey;

        public readonly string TicketGrantingServiceEncryptedTicket;

        public UserAuthenticationServerResponse(Guid ticketGrantingServiceId, int timestamp, int lifetime,
            byte[] ticketGrantingServiceSessionKey, string ticketGrantingServiceEncryptedTicket)
        {
            TicketGrantingServiceId = ticketGrantingServiceId;
            Timestamp = timestamp;
            Lifetime = lifetime;
            TicketGrantingServiceSessionKey = ticketGrantingServiceSessionKey;
            TicketGrantingServiceEncryptedTicket = ticketGrantingServiceEncryptedTicket;
        }

        public static UserAuthenticationServerResponse Decrypt(string encrypted, byte[] key)
        {
            return JsonConvert.DeserializeObject<UserAuthenticationServerResponse>(AesHelper.Decrypt(encrypted, key));
        }
    }
}