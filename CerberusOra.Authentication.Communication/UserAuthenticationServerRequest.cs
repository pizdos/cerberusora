﻿using System;
using System.Net;

namespace CerberusOra.Authentication.Communication
{
    public class UserAuthenticationServerRequest
    {
        public readonly Guid UserId;
        
        public readonly Guid ServiceId;
        
        public readonly IPAddress IpAddress = default;
        
        public readonly int LifetimeForTgtSeconds;

        public UserAuthenticationServerRequest(Guid userId, Guid serviceId, int lifetimeForTgtSeconds)
        {
            UserId = userId;
            ServiceId = serviceId;
            LifetimeForTgtSeconds = lifetimeForTgtSeconds;
        }
    }
}