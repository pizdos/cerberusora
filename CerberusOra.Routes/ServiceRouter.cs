﻿using System;
using System.Collections.Generic;
using System.Linq;
using CerberusOra.Service;

namespace CerberusOra.Routes
{
    public class ServiceRouter
    {
        private readonly Dictionary<string, SomeService> _ticketGrantingServices = new();
        
        private static readonly List<SomeService> DefaultServices = new()
        {
            new SomeService(Guid.Parse("7b9eb7b7-759b-429f-ad73-cc17b2e2109f"), "Vk"),
            new SomeService(Guid.Parse("aee10e45-faef-41cb-b15d-552f761ce64d"), "TikTok")
        };

        public ServiceRouter() : this(DefaultServices)
        {
        }

        public ServiceRouter(List<SomeService> ticketGrantingServices)
        {
            ticketGrantingServices.ForEach(service =>
                _ticketGrantingServices.Add(service.Name, service));
        }

        public List<SomeService> FindAll()
        {
            return _ticketGrantingServices.Values.ToList();
        }

        public SomeService Find(string name)
        {
            return _ticketGrantingServices[name];
        }
    }
}