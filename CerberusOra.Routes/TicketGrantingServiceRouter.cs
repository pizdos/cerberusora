﻿using System;
using System.Collections.Generic;
using System.Linq;
using CerberusOra.TicketGranting;

namespace CerberusOra.Routes
{
    public class TicketGrantingServiceRouter
    {
        private readonly Dictionary<Guid, TicketGrantingService> _ticketGrantingServices = new();

        private static readonly List<TicketGrantingService> DefaultTicketGrantingServices = new()
        {
            new TicketGrantingService(Guid.Parse("a57fcc83-b0dd-4ef0-be8b-0690e58d7d19"))
        };

        /*private readonly ServiceRouter _serviceRouter;*/

        public TicketGrantingServiceRouter(ServiceRouter serviceRouter) : this(DefaultTicketGrantingServices)
        {
            _ticketGrantingServices.First().Value.Register(serviceRouter.FindAll());
        }

        public TicketGrantingServiceRouter(List<TicketGrantingService> ticketGrantingServices)
        {
            ticketGrantingServices.ForEach(tgs =>
                _ticketGrantingServices.Add(tgs.Id, tgs));
        }

        public TicketGrantingService FindTicketGrantingService(Guid guid)
        {
            return _ticketGrantingServices[guid];
        }

        public TicketGrantingService First()
        {
            return _ticketGrantingServices.First().Value;
        }
    }
}