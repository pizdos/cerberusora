﻿using System;
using CerberusOra.Helpers;
using Newtonsoft.Json;

namespace CerberusOra.Abstractions.Models
{
    public class ServiceAuthenticator : IEncryptable
    {
        public readonly Guid ServiceId;
        public readonly int Timestamp;

        public ServiceAuthenticator(Guid serviceId, int timestamp)
        {
            ServiceId = serviceId;
            Timestamp = timestamp;
        }

        public static ServiceAuthenticator Decrypt(string encrypted, byte[] key)
        {
            return JsonConvert.DeserializeObject<ServiceAuthenticator>(AesHelper.Decrypt(encrypted, key));
        }
    }
}