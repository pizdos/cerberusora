﻿using System;
using CerberusOra.Helpers;
using Newtonsoft.Json;

namespace CerberusOra.Abstractions.Models
{
    public class UserAuthenticator : IEncryptable
    {
        public readonly Guid UserId;

        public readonly int Timestamp;

        public UserAuthenticator(Guid userId, int timestamp)
        {
            UserId = userId;
            Timestamp = timestamp;
        }

        public static UserAuthenticator Decrypt(string encrypted, byte[] key)
        {
            return JsonConvert.DeserializeObject<UserAuthenticator>(AesHelper.Decrypt(encrypted, key));
        }
    }
}