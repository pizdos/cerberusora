﻿using CerberusOra.Helpers;
using Newtonsoft.Json;

namespace CerberusOra.Abstractions
{
    public interface IEncryptable
    {
        string Encrypt(byte[] key) => AesHelper.Encrypt(Serialize(), key);
        
        string Serialize() => JsonConvert.SerializeObject(this);
    }
}