﻿using System;
using System.Collections.Generic;
using CerberusOra.Abstractions;
using CerberusOra.Authentication.Communication;
using CerberusOra.Helpers;
using CerberusOra.Routes;
using CerberusOra.SomeUser.Abstractions;
using CerberusOra.Tickets;

namespace CerberusOra.Authentication
{
    public class AuthenticationServer
    {
        private readonly Dictionary<Guid, byte[]> _userSecret = new(10);

        private readonly TicketGrantingServiceRouter _ticketGrantingServiceRouter;

        public AuthenticationServer(TicketGrantingServiceRouter ticketGrantingServiceRouter)
        {
            _ticketGrantingServiceRouter = ticketGrantingServiceRouter;
        }

        public string Auth(UserAuthenticationServerRequest request)
        {
            var ticketGrantingService = _ticketGrantingServiceRouter.First();
            var ticketGrantingServiceId = ticketGrantingService.Id;
            var timestamp = TimeSpanHelpers.NowSeconds();
            var lifetime = request.LifetimeForTgtSeconds;
            var ticketGrantingServiceSessionKey = Sha256Helpers.ComputeHash(Guid.NewGuid().ToString());
            
            IEncryptable ticket = new TicketGrantingServiceTicket(
                request.UserId, 
                ticketGrantingServiceId, 
                timestamp, 
                lifetime, 
                ticketGrantingServiceSessionKey
            );

            IEncryptable response = new UserAuthenticationServerResponse(
                ticketGrantingServiceId, 
                timestamp, 
                lifetime, 
                ticketGrantingServiceSessionKey,
                ticket.Encrypt(ticketGrantingService.Secret)
            );

            return response.Encrypt(_userSecret[request.UserId]);
        }

        public void Register(User user)
        {
            _userSecret.Add(user.Id, Sha256Helpers.ComputeHash($"{user.Password}{user.Name}"));
        }

        public void Register(IEnumerable<User> users)
        {
            foreach (var user in users) Register(user);
        }
    }
}