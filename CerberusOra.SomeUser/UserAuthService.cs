﻿using CerberusOra.Abstractions;
using CerberusOra.Abstractions.Models;
using CerberusOra.Authentication;
using CerberusOra.Authentication.Communication;
using CerberusOra.Routes;
using CerberusOra.Service.Communication;
using CerberusOra.SomeUser.Abstractions;
using CerberusOra.TicketGranting.Communication;

namespace CerberusOra.SomeUser
{
    public class UserAuthService
    {
        private readonly AuthenticationServer _authenticationServer;
        private readonly TicketGrantingServiceRouter _ticketGrantingServiceRouter;
        private readonly ServiceRouter _serviceRouter;

        public UserAuthService(AuthenticationServer authenticationServer,
            TicketGrantingServiceRouter ticketGrantingServiceRouter, ServiceRouter serviceRouter)
        {
            _authenticationServer = authenticationServer;
            _ticketGrantingServiceRouter = ticketGrantingServiceRouter;
            _serviceRouter = serviceRouter;
        }

        public ServiceAuthenticator TryAuthService(User user, string serviceName)
        {
            var service = _serviceRouter.Find(serviceName);

            var userAuthenticationServerRequest =
                new UserAuthenticationServerRequest(user.Id, service.Id, 123454321);
            var authenticationServerResponse = UserAuthenticationServerResponse.Decrypt(
                _authenticationServer.Auth(userAuthenticationServerRequest), user.Secret
            );

            IEncryptable userAuthenticator = new UserAuthenticator(user.Id, authenticationServerResponse.Timestamp);
            var ticketGrantingServiceSessionKey = authenticationServerResponse.TicketGrantingServiceSessionKey;
            var userTicketGrantingServiceRequest = new UserTicketGrantingServiceRequest(
                authenticationServerResponse.TicketGrantingServiceEncryptedTicket,
                userAuthenticator.Encrypt(ticketGrantingServiceSessionKey),
                service.Id,
                authenticationServerResponse.Lifetime);

            var ticketGrantingService = _ticketGrantingServiceRouter
                .FindTicketGrantingService(authenticationServerResponse.TicketGrantingServiceId);
            var userTicketGrantingServiceResponse = UserTicketGrangtingServiceResponse.Decrypt(
                ticketGrantingService.Auth(userTicketGrantingServiceRequest),
                ticketGrantingServiceSessionKey);
            user.ServiceTicketCache.GetOrAdd(service.Id, userTicketGrantingServiceResponse.EncryptedServiceTicket);
            var userServiceResponse = service.Auth(new UserServiceRequest(
                userTicketGrantingServiceResponse.EncryptedServiceTicket,
                userAuthenticator.Encrypt(userTicketGrantingServiceResponse.ServiceSessionKey)));

            var serviceAuthenticator = ServiceAuthenticator.Decrypt(
                userServiceResponse.EncryptedServiceAuthenticator,
                userTicketGrantingServiceResponse.ServiceSessionKey
            );

            return serviceAuthenticator;
        }
    }
}