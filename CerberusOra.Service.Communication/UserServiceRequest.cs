﻿namespace CerberusOra.Service.Communication
{
    public class UserServiceRequest
    {
        public readonly string EncryptedServiceTicket;

        public readonly string EncryptedUserAuthenticator;

        public UserServiceRequest(string encryptedServiceTicket, string encryptedUserAuthenticator)
        {
            EncryptedServiceTicket = encryptedServiceTicket;
            EncryptedUserAuthenticator = encryptedUserAuthenticator;
        }
    }
}