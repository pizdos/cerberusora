﻿namespace CerberusOra.Service.Communication
{
    public class UserServiceResponse
    {
        public readonly string EncryptedServiceAuthenticator;

        public UserServiceResponse(string encryptedServiceAuthenticator)
        {
            EncryptedServiceAuthenticator = encryptedServiceAuthenticator;
        }
    }
}