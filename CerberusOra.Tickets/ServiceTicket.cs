﻿using System;
using CerberusOra.Abstractions;
using CerberusOra.Helpers;
using Newtonsoft.Json;

namespace CerberusOra.Tickets
{
    public class ServiceTicket : IEncryptable
    {
        public readonly Guid UserId;

        public readonly Guid ServiceId;

        public readonly int Timestamp;

        public readonly int LifetimeSelf;

        public readonly byte[] ServiceSessionKey;

        public ServiceTicket(Guid userId, Guid serviceId, int timestamp, int lifetimeSelf, byte[] serviceSessionKey)
        {
            UserId = userId;
            ServiceId = serviceId;
            Timestamp = timestamp;
            LifetimeSelf = lifetimeSelf;
            ServiceSessionKey = serviceSessionKey;
        }

        public static ServiceTicket Decrypt(string encrypted, byte[] key)
        {
            return JsonConvert.DeserializeObject<ServiceTicket>(AesHelper.Decrypt(encrypted, key));
        }
    }
}