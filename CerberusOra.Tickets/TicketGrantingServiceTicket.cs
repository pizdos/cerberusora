﻿using System;
using CerberusOra.Abstractions;
using CerberusOra.Helpers;
using Newtonsoft.Json;

namespace CerberusOra.Tickets
{
    public class TicketGrantingServiceTicket : IEncryptable
    {
        public readonly Guid UserId;

        public readonly Guid TicketGrantingServiceId;

        public readonly int Timestamp;

        public readonly int Lifetime;

        public readonly byte[] TicketGrantingServiceSessionKey;

        public TicketGrantingServiceTicket(Guid userId, Guid ticketGrantingServiceId, int timestamp, int lifetime,
            byte[] ticketGrantingServiceSessionKey)
        {
            UserId = userId;
            TicketGrantingServiceId = ticketGrantingServiceId;
            Timestamp = timestamp;
            Lifetime = lifetime;
            TicketGrantingServiceSessionKey = ticketGrantingServiceSessionKey;
        }

        public static TicketGrantingServiceTicket Decrypt(string encrypted, byte[] key)
        {
            return JsonConvert.DeserializeObject<TicketGrantingServiceTicket>(AesHelper.Decrypt(encrypted, key));
        }
    }
}