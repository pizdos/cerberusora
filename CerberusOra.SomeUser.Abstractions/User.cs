﻿using System;
using CerberusOra.Helpers;
using Microsoft.Extensions.Caching.Memory;

namespace CerberusOra.SomeUser.Abstractions
{
    public class User
    {
        public readonly Guid Id;

        public readonly string Name;

        public readonly string Password;

        public readonly Cache<string> ServiceTicketCache = new(
            new MemoryCacheEntryOptions()
            {
                Size = 1,
                Priority = CacheItemPriority.High,
                SlidingExpiration = TimeSpan.FromMinutes(60)
            });

        public byte[] Secret =>
            Sha256Helpers.ComputeHash($"{Password}{Name}");

        public User(
            Guid id,
            string name,
            string password
        )
        {
            Id = id;
            Name = name;
            Password = password;
        }
    }
}