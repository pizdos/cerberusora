﻿using System;
using CerberusOra.Abstractions;
using CerberusOra.Helpers;
using Newtonsoft.Json;

namespace CerberusOra.TicketGranting.Communication
{
    public class UserTicketGrangtingServiceResponse : IEncryptable
    {
        public readonly Guid ServiceId;

        public readonly int Timestamp;

        public readonly int Lifetime;

        public readonly byte[] ServiceSessionKey;

        public readonly string EncryptedServiceTicket;


        public UserTicketGrangtingServiceResponse(
            Guid serviceId,
            int timestamp,
            int lifetime,
            byte[] serviceSessionKey,
            string encryptedServiceTicket
        )
        {
            ServiceId = serviceId;
            Timestamp = timestamp;
            Lifetime = lifetime;
            ServiceSessionKey = serviceSessionKey;
            EncryptedServiceTicket = encryptedServiceTicket;
        }
        
        public static UserTicketGrangtingServiceResponse Decrypt(string encrypted, byte[] key)
        {
            return JsonConvert.DeserializeObject<UserTicketGrangtingServiceResponse>(AesHelper.Decrypt(encrypted, key));
        }
    }
}