﻿using System;

namespace CerberusOra.TicketGranting.Communication
{
    public class UserTicketGrantingServiceRequest
    {
        public readonly string EncryptedTicket;
        
        public readonly string UserAuthenticator;

        public readonly Guid ServiceId;

        public UserTicketGrantingServiceRequest(string encryptedTicket, string userAuthenticator, Guid serviceId, int lifetime)
        {
            EncryptedTicket = encryptedTicket;
            UserAuthenticator = userAuthenticator;
            ServiceId = serviceId;
            Lifetime = lifetime;
        }

        public readonly int Lifetime;
    }
}